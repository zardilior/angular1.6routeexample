angular.module("routing").config(["$routeProvider","$locationProvider",function($routeProvider,$locationProvider){
    $locationProvider.hashPrefix('');
    $routeProvider.when("/main", {
        templateUrl: "main.html",
        controller:"main"
    }).when("/second",{
        templateUrl: "second.html",
        controller:"main"
    })/*.otherwise("/main")*/;
}]);
